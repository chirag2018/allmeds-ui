import React, {Component } from 'react';
import Footer from "../footer";

var str = "Hello world!";

var res = str.substring(1, 4);

class Strip extends Component {
    state = {
        qr:"23231",
        medicine:"Paracetamol",
        result:{},
        valid:"",
        manufactureDate:"",
        expireDate:""
      }
      qrOnChnage=(e)=>{

          this.setState({qr:e.target.value});
      };
      medicineOnChnage=(e)=>{
            this.setState({medicine:e.target.value})
      };
      validation=(e)=>{
          let flag=false;
        fetch("http://localhost:8085/allmeds/drugs/"+this.state.medicine+"/"+this.state.qr,{
            method:"GET",
            header:{
                "content-type":"application/json"
            },
            body:JSON.stringify(this.props.skill)
            })
                .then(res=>{
                    
                    console.log(res.status)
                    return res.json()})
                    .then(data=>{
                        if(data.valid===true){
                            this.setState({valid:"YES"})
                        }
                        else if(data.valid===false){
                            this.setState({valid:"NO"})
                        }
                        console.log("RESULT : ",data.drugsDetails);
                            this.setState({result:data.drugsDetails,
                                manufactureDate:data.drugsDetails.manufacturingDate,
                                expireDate:data.drugsDetails.expiringDate
                            });
                        
                        
                    }
                        );
      };
    render() { 
        let classes="badge m-2 badge-";
        classes+=(this.state.valid==="NO")?"danger":"success";
        return ( 
            

            <div className="container">
           


            <div class="card">
  <div class="card-header">
    Validate Medicine Here 
  </div>
  <div class="card-body">
    <h5 class="card-title"></h5>
    <div className="row form-group">
    <div className="col-md-1"></div>
                <div className="col-md-2">
                <label>Enter QR/BAR code</label>
                </div>
                <div className="col-md-3 col-sm-12">
                    <input type="text" value={this.state.qr}  onChange={this.qrOnChnage} placeholder="Enter Code"/>
                </div>
                <div className="col-md-2">
                <label>Enter Medicine</label>
                </div>
                <div className="col-md-2 col-sm-12">
                    <input type="text" value={this.state.medicine}  onChange={this.medicineOnChnage} placeholder="Enter Medicine Name"/>
                </div>
            </div>

   
            <div className="row">
            <div className="col-md-2">
            
            </div>
            <div className="col-md-8">
            
                            <div class="card text-center">
               
                <div class="card-body">
                    <h5 className="card-title"><button className="btn btn-primary btn-sm" onClick={this.validation}>Validate</button></h5>
                    <table align="center">
                        <tr>
                            <td align="left">Medicine Name</td>
                            <td>:</td>
                            <td align="center">{this.state.medicine}</td>
                        </tr>
                        <tr>
                            <td align="left">Manifacture Date</td>
                            <td>:</td>
                            <td align="center">{this.state.manufactureDate.substring(0, 10)}</td>
                        </tr>
                        <tr>
                            <td align="left">Expire Date</td>
                            <td>:</td>
                            <td align="center">{this.state.expireDate.substring(0, 10)}</td>
                        </tr>
                        <tr>
                            <td align="left">Is medicine valid ?</td>
                            <td>:</td>
                            <td className={classes} align="center">{this.state.valid}</td>
                        </tr>
                    </table>
                </div>
                <div class="card-footer text-muted">
                    <img className="form-group" src="https://www.nalashaa.com/wp-content/uploads/2018/04/nes-logo.png" width="200px" height="50px"/> 
                </div>
                    </div>
            </div>
            </div>
    
  </div>
</div>
<Footer/>
            </div>
        );
    }
}
 
export default Strip;