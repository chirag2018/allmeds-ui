import React, { Component } from 'react';
import {browserHistory} from "react-router";
import Autocomplete  from "react-autocomplete";
import {connect} from "react-redux";
import store from "../../store";
import Footer from "../footer";;

var sectionStyle = {
    width: "100%",
    height: "550px",
    backgroundImage: "url(https://www.formstack.com/blog/wp-content/uploads/2016/11/ITimprovingHealthcare.jpg)"
  };
  
  

class Login extends Component {
    state = { 
        user:{
            userName:"",
            password:""
        },
        result:{}
     }

     getAutoComplete=()=>{
       
     };


     logInUser=(userData)=>{
         
         console.log("SENDING ",userData)
        const headers=new Headers();
        headers.append('Content-Type', 'application/json');
        const options={
            method:"POST",
            headers,
            body:JSON.stringify(userData),
        };
        const request=new Request('http://localhost:8085/allmeds/users/login',options);
        const response= fetch(request)
        .then(resp=>{
            console.log("RES",resp);
            if(resp.status===200){
              // browserHistory.push("/userDetail");
            }
            console.log(resp)
           return resp.text();
        }).then(data=>{
            console.log("Response + ",data)
            if(data===""){
                alert("In-valid UserName or password !!")
                this.setState({userName:"",
            password:""
            })
            }
            else{
                this.setState({result:data});
console.log("SETTING USER LOGIN");
                store.dispatch({
                    type:"SET_USER_DATA",
                    payload:JSON.parse(data)
                })
                { browserHistory.push("/userDetail");}
            }
            
        }
            // data.length ? (this.setState.result=JSON.parse(data)) : {},
            );
        const status=response.status;
        if(this.state.result==="null"){
        }
     };

     onChangeValue=(e)=>{
        this.setState({user:{
            ...this.state.user,
             [e.target.name]:e.target.value}
         })
     };
    render() { 
        return (  <div clssName="container form-group">
            
            <section style={ sectionStyle }>
            <br></br>
            <br></br>
            <div className="row">
            <div className="col-md-4"></div>
                <div className="col-md-6 col-sm-6">
                    <h3>Patient login</h3>
                </div>
            </div><br></br>
            <div className="row form-group">
                <div className="col-md-2"></div>
                <div className="col-md-2 col-sm-6 col-xs-12">
                    <label for="userName">User Name</label>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="userName" value={this.state.user.userName} id="userName" placeholder="Enter User Name" onChange={this.onChangeValue}/>
                </div>
            </div>
            <div className="row form-group">
                <div className="col-md-2"></div>
                <div className="col-md-2 col-sm-6 col-xs-12">
                    <label for="password">User Password</label>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-12">
                    <input type="password" name="password" value={this.state.user.password} id="password" placeholder="Enter Password" onChange={this.onChangeValue}/>
                </div>
            </div>

            <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-1">
                <button className="btn btn-primary btn-sm" onClick={()=>this.logInUser(this.state.user)}>Login</button>
                </div>
                <div className="col-md-3">
                <a className="text-warning" href="/patientDetail">Don't have Account?</a>
                </div>
            </div>
           
      </section>
      <Footer/>
        </div>);
    }
}
const mapStateToProps=(state)=>{
    return {
        user:state.userReducer
    };
};


const mapDispatchToProps=(dispatch)=>{
    return {
      
        setUser:()=>{
            console.log("SKILL is ");
            dispatch({
                type:"SET_USER_DATA",
                payload:{
                    disease:"Fever",
                    drugsList:[{
                        drugName:"Drug 1",
                        ingredients:[
                            "abc",
                            "123"
                        ],
                    },
                    {
                        drugName:"Drug 2",
                        ingredients:[
                            "xyz",
                            "000"
                        ],
                    }

                ],
                    doAnddont:" Do and Dont will be displayd"
                        // name:"Fever",
                        // medicineName:["abc","xyz"], 
                        // description:"Fever is when a human's body temperature goes above the normal range of 36–37° Centigrade (98–100° Fahrenheit). It is a common medical sign",
                        // do:"Take Tablets",
                        // dont:"Dont take bath"
                }
            });
        }
    };
};

export default (Login);