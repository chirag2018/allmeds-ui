    import React, { Component } from 'react';
    import axios from 'axios'
    import {browserHistory} from "react-router";
    import "../style/commonStyle.css";
    import Footer from "../footer";

class patientDetail  extends Component {
    state = { 
        userDetails:{
            userName:"",
            dob:"",
            phNo:"",
            address:"",
            password:"",
            gender:"",
            allergic:"",
            name:"",
            id:"",
            height:"",
            weight:""


        }
     }
     onChangeValues=(e)=>{
         this.setState({userDetails:{
            ...this.state.userDetails,
             [e.target.name]:e.target.value}
         })
     };


     sendDetails=(userData)=>{
         console.log("SENDING ",userData);
         const headers=new Headers();
         headers.append('Content-Type', 'application/json');

         const options={
             method:"POST",
             headers,
             body:JSON.stringify(userData),
         };
         let flag=false;
         const request=new Request('http://localhost:8085/allmeds/users/register',options);
         const response= fetch(request)
         .then(resp=>{
             console.log("RES",resp.status);
             if(resp.status===200){
                flag=true;
             }
            return resp.text();
         }).then(data=>{
            if(flag===false){
                alert("User Coudn't be Created !!")
            }
            else{
                browserHistory.push("/userLogin");
            }
            console.log("RESPONSE",data)
            
            });
         const status=response.status;

// const axios = require('axios');
// axios.post('http://localhost:8081/healthcareskillmatrix/skillCategories/userDeatils',userData)
//   .then(function (response) {
//     console.log(response);
//   })
//   .catch(function (error) {
//     console.log(error);
//   });

        // fetch("http://localhost:8081/healthcareskillmatrix/skillCategories/userDeatils?",{
        //     method:"POST",
        //     json: true,
        //     body:JSON.stringify({userData}),
        //     header:{  'Authorization': `bearer `,
        //     'Content-Type': 'application/json',}
        //     })
        //         .then(response=>{

        //             return response.json()})
        //             .then(data=>console.log("RESULT : ",data));
     };
    render() { 
        return ( 
        <div>
            <div className="container">
                <h2 >Patient Details</h2>
                <div className="row">
                    <div className="col-md-6 col-sm-12">
                        <div className="row container">
                       <img src="https://www.formstack.com/blog/wp-content/uploads/2016/11/ITimprovingHealthcare.jpg" width="500px" height="520px"/>
                        </div>
                    </div>

                    <div className="col-md-6 col-sm-12">
                    <div class="card">
                    <div class="card-header">
                        <p align="center">Patient Details</p>
                    </div>
                         <div class="card-body">

                         <div className="row form-group">
                                <div className="col-md-4 col-sm-4">
                                        <label className="a" for="userName">User Name</label>
                                    </div>
                                    <div className="col-md-8 col-sm-8">
                                        <input type="text" name="userName" value={this.state.userDetails.userName}  placeholder="Enter User Name" onChange={this.onChangeValues}/>
                                    </div>
                                </div>

                            <div className="row form-group">
                                <div className="col-md-4 col-sm-4">
                                        <label className="a" for="name">Patient Name</label>
                                    </div>
                                    <div className="col-md-8 col-sm-8">
                                        <input type="text" name="name" value={this.state.userDetails.name}  placeholder="Enter Patient Name" onChange={this.onChangeValues}/>
                                    </div>
                                </div>

                                <div className="row form-group">
                                <div className="col-md-4 col-sm-4">
                                        <label for="age">Patient DOB</label>
                                    </div>
                                    <div className="col-md-8 col-sm-8">
                                        <input type="date" value={this.state.userDetails.dob}  name="dob" placeholder="Enter Patient Age"  onChange={this.onChangeValues}/>
                                    </div>
                                </div>

                                <div className="row form-group">
                                <div className="col-md-4 col-sm-4">
                                        <label for="phNo">Patient Ph.No</label>
                                    </div>
                                    <div className="col-md-8 col-sm-8">
                                        <input type="text" name="phNo" value={this.state.userDetails.phNo} placeholder="Enter Patient Ph.No" onChange={this.onChangeValues}/>
                                    </div>
                                </div>

                                <div className="row form-group">
                                <div className="col-md-4 col-sm-4">
                                        <label for="address">Patient Address</label>
                                    </div>
                                    <div className="col-md-8 col-sm-8">
                                        <input type="text" name="address" value={this.state.userDetails.address} placeholder="Enter Patient Name" onChange={this.onChangeValues}/>
                                    </div>
                                </div>

                                <div className="row form-group">
                                <div className="col-md-4 col-sm-4">
                                        <label for="password">Create Password</label>
                                    </div>
                                    <div className="col-md-8 col-sm-8">
                                        <input type="text" name="password" value={this.state.userDetails.password} placeholder="Enter New Password" onChange={this.onChangeValues}/>
                                    </div>
                                </div>

                                

                                <div className="row form-group">
                                    <div className="col-md-4 col-sm-6">
                                        <label for="gender">Gender</label>
                                    </div>
                                <div className="col-md-2 col-sm-2">
                                      Male <input type="radio" name="gender" value="male" onChange={this.onChangeValues}/>
                                </div>
                                <div className="col-md-3 col-sm-4">
                                      Female <input type="radio" name="gender" value="female" onChange={this.onChangeValues}/>
                                </div>
                                </div>

                                <div className="row form-group">
                                    <div className="col-md-4 col-sm-6">
                                        <label for="allergic">Allergic</label>
                                    </div>
                                    <div className="col-md-4 col-sm-6">
                                            <textarea value={this.state.allergic} name="allergic" id="allergic" cols="24" rows="1" onChange={this.onChangeValues}></textarea>
                                    </div>
                                </div>

                             <div className="row form-group">
                                    <div className="col-md-4 col-sm-6">
                                        <label for="height">Height</label>
                                    </div>
                                    <div className="col-md-4 col-sm-6">
                                           <input type="number"  name="height" id="height" value={this.state.height} onChange={this.onChangeValues}/>
                                    </div>
                                </div>

                            <div className="row form-group">
                                    <div className="col-md-4 col-sm-6">
                                        <label for="weight">Weight</label>
                                    </div>
                                    <div className="col-md-4 col-sm-6">
                                           <input type="number"  name="weight" id="weight" value={this.state.weight} onChange={this.onChangeValues}/>
                                    </div>
                                </div>
                            <div className="row">
                                <div className="col-md-4">
                                <button className="btn btn-primary btn-sm" onClick={()=>this.sendDetails(this.state.userDetails)}>Sign up</button>
                                </div>
                                <div className="col-md-6">
                                <a className="text-warning" href="/userLogin">Already have Account?</a>
                                </div>

                            </div>
                           
                            </div>
                       </div>
                   
                    </div>

                </div>    
            </div>
            <Footer/>
        </div> );
    }
}
 
export default patientDetail;