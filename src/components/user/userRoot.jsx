import React, { Component } from 'react';
import Header from '../logInHeader';

class UserRoot extends Component {
    state = {  }
    render() { 
        return (
            <div>
        <div className="container">
                <div className="col-md-12">
                <Header/>        
                </div>
        </div>

        <div className="row">
            <div className="col-md-12">
                {this.props.children}        
             </div>
        </div>
        </div>
         );
    }
}
export default UserRoot;