import React, { Component } from 'react';
import {browserHistory} from "react-router";
import "../style/commonStyle.css";
import SearchDisease from "../ReduxPage";
import store from "../../store";
var sectionStyle = {
    backgroundSize: 'cover',
    width: "100%",
    height: "600px",
    backgroundImage: "url()"
  };
  let dateOfBirth=store.getState().userReducer.loginData.dob;
 
class UserDetails extends Component {
    state = {  
        logInData:store.getState().userReducer.loginData,
        userDetails:{
        userName:"",
        dob:"",
        phNo:"",
        address:"",
        gender:"",
        allergic:"",
        height:"",
        weight:""
    }
    
 }
 
 onChangeValues=(e)=>{
     console.log(this.state.logInData)
     this.setState({userDetails:{
        ...this.state.userDetails,
         [e.target.name]:e.target.value}
     })
 };


 sendDetails=(userData)=>{
     console.log("SENDING ",userData);
     const headers=new Headers();
     
     headers.append('Content-Type', 'application/json');

     const options={
         method:"POST",
         headers,
         body:JSON.stringify(userData),
     };
     let flag=false;
     const request=new Request('http://localhost:8081/healthcareskillmatrix/skillCategories/userDeatils',options);
     const response= fetch(request)
     .then(resp=>{
         console.log("RES",resp.status);
         if(resp.status!==200){
            flag=false;
         }
        return resp.text();
     }).then(data=>{
        if(flag===false){
            alert("User Coudn't be Created !!")
        }
        else{
            browserHistory.push("/userLogin");
        }
        console.log("RESPONSE",data)
        
        });
     const status=response.status;

    };
    render() { 
         
        return ( 
            <div>
                <div className="container">
                    <h2 className="text-primary" align="center">Patient Details</h2>
                    
                    <div className="row">
                        {/* <div className="col-md-3 col-sm-12">
                            <div className="row container">
                           <img src="https://www.formstack.com/blog/wp-content/uploads/2016/11/ITimprovingHealthcare.jpg" width="500px" height="520px"/>
                            </div>
                        </div> */}
   
                        <div  className="col-md-12 col-sm-12">
                        <div  style={ sectionStyle }  className="card">
                        <div className="card-header">
                            Patient Detail
                        </div>
                             <div class="card-body">
                                <div className="row form-group">
                                    <div className="col-md-2 col-sm-4">
                                            <label for="userName">Patient Name</label>
                                        </div>
                                        <div className="col-md-5 col-sm-8">
                                            <input type="text" name="userName" value={this.state.logInData.name}  placeholder="Enter Patient Name" onChange={this.onChangeValues} disabled/>
                                        </div>
                                    
                                    <div className="col-md-2 col-sm-4">
                                            <label for="age">Patient DOB</label>
                                        </div>
                                        <div className="col-md-3 col-sm-8">
                                            <input type="text" value={dateOfBirth}  name="dob" placeholder="Enter Patient Age"  disabled/>
                                        </div>
                                    </div>
    
                                    <div className="row form-group">
                                    <div className="col-md-2 col-sm-4">
                                            <label for="phNo">Phone.No</label>
                                        </div>
                                        <div className="col-md-5 col-sm-8">
                                            <input type="text" name="phNo" value={this.state.logInData.phNo} placeholder="Enter Patient Ph.No" onChange={this.onChangeValues} disabled/>
                                        </div>
                                 
                                    <div className="col-md-2 col-sm-4">
                                            <label for="address">Address</label>
                                        </div>
                                        <div className="col-md-2 col-sm-8">
                                            <input type="text" name="address" value={this.state.logInData.address} placeholder="Enter Patient Address" onChange={this.onChangeValues} disabled/>
                                        </div>
                                    </div>
    
    
                                    <div className="row form-group">
                                        <div className="col-md-2 col-sm-6">
                                            <label for="gender">Gender</label>
                                        </div>
                                    <div className="col-md-1 col-sm-2">
                                          Male <input type="radio" name="gender" value="male" onChange={this.onChangeValues}/>
                                    </div>
                                    <div className="col-md-4 col-sm-4">
                                          Female <input type="radio" name="gender" value="female" onChange={this.onChangeValues}/>
                                    </div>
                                    
                                        <div className="col-md-2 col-sm-6">
                                            <label for="allergic">Allergic</label>
                                        </div>
                                        <div className="col-md-3 col-sm-6">
                                                <textarea value={this.state.allergic} name="allergic" id="allergic" cols="24" rows="1" placeholder="Enter Patient Allergic" onChange={this.onChangeValues}></textarea>
                                        </div>
                                    </div>
    
                                 <div className="row form-group">
                                        <div className="col-md-2 col-sm-6">
                                            <label for="height">Height</label>
                                        </div>
                                        <div className="col-md-5 col-sm-6">
                                               <input type="number"  name="height" id="height" value={this.state.height} placeholder="Enter Patient Height" onChange={this.onChangeValues}/>
                                        </div>
                                    
                                        <div className="col-md-2 col-sm-6">
                                            <label for="weight">Weight</label>
                                        </div>
                                        <div className="col-md-3 col-sm-6">
                                               <input type="number"  name="weight" id="weight" value={this.state.weight} placeholder="Enter Patient Weight" onChange={this.onChangeValues}/>
                                        </div>
                                    </div>
                                
                                <button className="btn btn-primary btn-sm" onClick={()=>this.sendDetails(this.state.userDetails)}>Sign up</button>
                                <hr></hr><hr></hr>
                                </div>
                                <footer class="blockquote-footer"> <SearchDisease/></footer>
                               
                           </div>
                       
                        </div>
    
                    </div>    
                </div>
                
                <div></div>
                {/* <label class="field a-field a-field_a1 page__field">
          <input class="field__input" placeholder="e.g. Stanislav" name="userName" value={this.state.userName}  required/>
          <span class="field__label-wrap">
            <span class="field__label">First name</span>
          </span>
        </label> */}
        
        
       

            </div> );
    }
}
 
export default UserDetails;