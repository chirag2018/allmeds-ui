import React, { Component } from 'react';
import {connect} from "react-redux";
import ReactTable from 'react-table';
import store from "../store";

class ReduxApp extends Component {
    state = {
        info:[],
        category:"a",
        skills:[],
        saveSkill:{
            skillCategory:""
            
        },
        skillCategory:"",
        skill:"",
        relaventExperience:"",
        rating:"Beginner"
      }
      
      constructor(props){
          super(props);
         // this.deleteSelectedSkill=this.deleteSelectedSkill.bind(this);
      }
     
    onChangeHandle=(e)=>{
        let s=[];
       console.log("Target ",e.target.value);
       let str=e.target.value;
       this.setState({
        skillCategory:e.target.value,
        skill:"",
         relaventExperience:""
       })

       console.log("Selected Category : ");
       
       for(let index=0;index<this.state.info.length;index++){
           if(this.state.info[index].categoryName===str){
                 //  alert("Category Name found :"+this.state.info[index].categoryName);
                   for(let i=0;i<this.props.allSkill.skillInfo[index].skills.length;i++){
                       s[i]=this.state.info[index].skills[i];
                   }
           }
       }
     s.sort(function(obj1,obj2){
         return obj1.id-obj2.id;
     })
       this.setState(this.state.skills=s);
      
    //    this.setState({saveSkill:{skillCategory:document.getElementById("category").value}
    // },()=>{
    //     console.log("Category Name ",this.state.saveSkill.skillCategory);
    // });
   
   };

   saveHandle=(data)=>{
    console.log("Data sending to server : ",this.props.skill.skill);
    fetch("http://192.168.2.93:8081/healthcareskillmatrix/skillCategories/sampleSkill?",{
        method:"POST",
        header:{
            "content-type":"application/json"
        },
        body:JSON.stringify(this.props.skill)
        })
            .then(res=>res)
                .then(data=>console.log("RESULT : ",data));
    };


   componentDidMount(){
    fetch('http://192.168.7.64:8081/healthcareskillmatrix/skillCategories')
        .then(results => results.json())
            .then(data =>{ 
                this.setState({info:data});
                this.props.setAllSkill(this.state);
            });
                
            //  this.setState(this.state.info:store.skillCategories.skillInfo)  
}

changeState=(event)=>{
this.setState({
    rating:event.target.value
})
};

setSkilState=(event)=>{
    let filteredArray = this.state.skills.filter(item => item !== event.target.value)
    this.setState({skills: filteredArray});
this.setState({
    skill:event.target.value
})
let index=event.target.selectedIndex-1;
console.log("Events ",event.target)
console.log(index)

//console.log("DATA ",this.props.allSkill.skillInfo[1].skills.slice(index,1));
//console.log("DATA ",this.props.allSkill.skillInfo.find(k => k =='Operating Systems'));
console.log("INDEX",event.target.selectedIndex);
let i=event.target.selectedIndex-1;
this.state.info[0].skills.filter(e1=>e1!==event.target.value)
console.log(this.state.info)
};

setExpState=(event)=>{
    console.log("After filter ",this.state.skills);
this.setState({
    relaventExperience:event.target.value
})
//document.getElementById("rating").disabled=false;
};


setText=(event)=>{
    
};

validateRating=()=>{
    return true;
};

deleteSkill=(e)=>{
// console.log("INDEX",e.target.selectedIndex);
// let i=e.target.selectedIndex-1;
//let deletedItem=this.props.allSkill.skillInfo[0].skills.splice(0,1);


}
    render() { 
        

        return ( <div className="container">
            <div className="row">
                    <div className="col-md-2">
                        </div>
                    <div className="col-md-4">
                    {/* <button className="btn-primary btn-sm"onClick={this.props.setName}>Add Skill</button> */}
                    </div>
            </div>
            
            
{/* <div className="row">           WORKING
     <div className="col-md-4">
             <button className="btn-primary btn-sm" onClick={this.saveHandle}>Save Skills</button>
     </div>
 </div> */}
        
            <div className="panel-body">

						<div className="row form-group">
							<div className="col-md-3 col-sm-12">
								<select onChange={this.onChangeHandle} className="form-control" id="category"  data-toggle="tooltip" data-placement="top" title="Choose Category">

								<option>----- Choose Category -----</option>
                                {this.props.allSkill.skillInfo.map(function (item,index){
                   return (<option  key={item.categoryName} value={item.categoryName}  >{item.categoryName}</option>)
                })}
                </select>
							</div>
							<div className="col-md-3 col-sm-12">
								<select onChange={this.setSkilState} onClick={this.deleteSkill}  className="form-control" id="skills"  data-toggle="tooltip" data-placement="top" 
								title="Choose Skills" disabled={!this.state.skillCategory}>

								<option>----- Choose Skills -----</option>
                                {this.state.skills.map(function (item,index){
                                    return (<option id={index} key={item.skill+index} value={item.skill}  >{item.skill}</option>)
                                    })}
                               </select>
							</div>
							<div className="col-md-2 col-sm-12">
								<input className="form-control" type="number" id="experience"
									name="experience" placeholder="Enter experience" min="0"
									max="50" value={this.state.relaventExperience} data-toggle="tooltip" data-placement="top" title="Enter Experience" onChange={this.setExpState}
                                    disabled={!this.state.skill}/>

							</div>
							<div className="col-md-2 col-sm-12">
                                <select onClick={this.changeState} className="form-control" id="rating"  data-toggle="tooltip" data-placement="top"
                                 title="Choose Rating" disabled={!this.state.relaventExperience}>
                                    <optgroup label = "----- Choose Rating -----">
                                    <option>Beginner</option>
									<option>Intermediate</option>
									<option>Proficient</option>
									<option>Expert</option>
                                    </optgroup>
								</select>
							</div>
{/* <p>{this.props.allSkill.skillInfo.map(function(item,index){
    return item.categoryName;
})}</p> */}
                             <div className="col-md-1">
                                        <button className="btn btn-primary btn-sm" onClick={()=>this.props.saveSkill(this.state)}>Save</button>
                                        </div>
                                        <div className="row">
                                        {/* <button className="btn btn-primary btn-sm" onClick={()=>this.props.setAllSkill(this.state)}>Add</button> */}
                                        </div>
						</div>
						
					</div><hr/>


        
        </div> );
    }
}

const mapStateToProps=(state)=>{
    return {
        skill:state.skillReducer,
        allSkill:state.skillCategories
    };
};

const mapDispatchToProps=(dispatch)=>{
    return {
      
        saveSkill:(skill)=>{
            console.log("SKILL is ",skill.skillCategory);
            dispatch({
                type:"SAVE_SKILL",
                payload:{
                    category:skill.skillCategory,
                    skillCategory:skill.skill,
                    relaventExperience:skill.relaventExperience,
                    rating:skill.rating
                }
            });
        },
        setAllSkill:(allSkill)=>{
            console.log("Setting skills");
            dispatch({
                type:"SET_SKILL",
                payload:allSkill.info
            });
        }
    };
};

export default connect(mapStateToProps,mapDispatchToProps)(ReduxApp);
  