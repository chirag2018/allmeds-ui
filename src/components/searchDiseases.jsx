import React, { Component } from 'react';
import {connect} from "react-redux";
import Footer from "./footer";

var pageBackgroundStyle = {
    backgroundSize: 'cover',
    width: "100%",
    height: "100%",
    backgroundImage: "url( https://media.licdn.com/media/gcrc/dms/image/C5112AQEAsqicCV6HHw/article-cover_image-shrink_720_1280/0?e=1548288000&v=beta&t=i8kD8GBiG9tmd8lmn_FM2SHM27VEcKw-2beF5QZyynU)"
  };

class SearchDiseases extends Component {
    state = { 
        Diseases:{
            name:"",
            medicineName:[], 
            description:"",
            do:"",
            dont:""
        },
        response:[],
        showData:{},
        drugsList:[],
        ingredients:[],
        level:""
       
     }
     componentDidMount(){
         this.props.saveHospital();
         this.props.saveDisease("");
         //this.props.saveDisease("");

     }

     onChangeDisease=(e)=>{
        
for(let i=0;i<this.props.disease.response.length;i++){
    if(this.props.disease.response[i].disease===e.target.value){
        this.setState({drugsList:this.props.disease.response[i].drugsList})
        this.setState({showData:this.props.disease.response[i]});
        
    }
    if(e.target.value==="-- Choose Disease --")
    {
       // this.setState({showData:""});
    }
}
this.setState({level:"0"})
if(e.target.value==="Jaundice"){
    this.setState({level:"2"})
   // alert("You have Level 2. Please Find near by hospital")
}
if(e.target.value==="Heart Attack"){
    this.setState({level:"3"})
   // alert("You have Level 3. Please Find near by hospital")
}
     };
     getMedicineNames=()=>{
         console.log("Hospital ",this.props.hospital.hospital[0].hospital.name)
         if(this.state.level==="2"){
             return (<div>
                  {this.props.hospital.hospital.map(function (item,index){
                                    return (
                                        <div>
                                    <tr>
                                        <td className="text-info">Hospital Name : </td>
                                        <td className="text-danger"  key={index} value={item.hospital.name}  >{item.hospital.name}</td>
                                        <td>   </td>
                                        <td className="text-info">Hospital number : {item.hospital.contact}</td>
                                       
                                    </tr>
                                        

                                    </div>
                                    )
                                    })}
                                        <hr></hr>
                                    <div className="row">
                                    <div className="col-md-7"></div>
                                            <div className="col-md-4">
                                            <div className="card text-center">
                                                    <div class="card-header bg-info">
                                                        Chat with doctor
                                                    </div>
                                                    <div class="card-body">
                                                       
                                                    <div align="left"><p  className="badge badge-primary">Andy</p> <p className="card-text">Hi</p></div>
                                                    <div align="right"><p  className="badge badge-primary">John</p> <p className="card-text">Hello</p></div>

                                                    </div>
                                                    <div className="card-footer text-muted">
                                                       <input type="text" name="message" placeholder="Type message here"/>
                                                       <button className="btn btn-primary btn-sm">send</button>
                                                    </div>
                                                   
                                                    </div>
                                            
                                            </div>
                                        </div>
             </div>);
         }
         if(this.state.level==="0"){
             return (<div>
                <span></span>
             </div>);
         }
         if(this.state.level==="3"){
            return (<div>
               <span></span>
            </div>);
        }
       

     };
     redirectToShop=()=>{
         
        window.open("https://www.netmeds.com", "Medicine")
     };
     disaplyDiscription=()=>{
         
         return(
         <div className="container">
             <div className="card">
             <h4 className="card-header">Diseases Details</h4>
                <div className="card-body">
                    
                    <h6 className="card-subtitle">Diseases Name : <span className="text-info">{this.state.showData.disease}</span></h6>
                    <div className="table table-responsive">
                       
                        <tr className="card-text">
                                <td className="text-info">Do</td>
                                <td>:</td>
                                <td>{this.state.showData.doAnddont}</td>
                        </tr>
                        <tr className="card-text">
                                <td className="text-info">Don't</td>
                                <td>:</td>
                                <td>{this.state.showData.dont}</td>
                        </tr>
                        {/* <tr className="card-text">
                                <td>Tablets</td>
                                <td>:</td>
                                <td>{this.state.showData.description}</td>

                                {this.state.drugsList.map(function (item,index){
                                    return (<td  key={index} value={item.drugName}  >{item.drugName}</td>)
                                    })}
                                   
                                {console.log("DRUGS ARE ",this.state.drugsList)}
                        </tr> */}
                      
                      
                         { this.state.level!=="3" ? <div>
                         {this.state.drugsList.map(function (item,index){
                                    return (
                                        <div>
                                    <tr>
                                        <td className="text-info">Drug Name : </td>
                                        <td  key={index} value={item.drugName}  >{item.drugName}</td>
                                        <td>   </td>
                                            <td className="text-info">ingredients are : </td>
                                        {item.ingredients.map(function(item,i){
                                            return(<td key={i} value={item}>{item} </td>)
                                        })}
                                    </tr>
                                    </div>
                                    )
                                    })}
                         </div> : <div>
                            
                         </div> }
                      
                         { this.state.level==="3" ? <div>
                             <h4> <span className="badge badge-warning">Please Conatct hospitals</span></h4>
                         {this.props.hospital.hospital.map(function (item,index){
                                    return (
                                        <div>
                                    <tr>
                                        <td className="text-info">Hospital Name : </td>
                                        <td className="text-danger"  key={index} value={item.hospital.name}  >{item.hospital.name}</td>
                                        <td>  : </td>
                                            <td className="text-info">Hospital number : {item.hospital.contact}</td>
                                    </tr>
                                    </div>
                                    )
                                    })}
                         </div> : <div></div> }
                        
                        {this.getMedicineNames()}
                       
                        
                    </div>
                </div>
            </div>

            <div className="card">
                <div className="card-body">
                <div className="row">
                    <div className="col-md-3 col-sm-3">
                    <a href="validate">  <button  className="btn btn-info btm-sm">Validate Medicine</button></a>
                    </div>
                    <div className="col-md-3 col-sm-3">
                        <button className="btn btn-info btm-sm" onClick={this.redirectToShop}>Order Medicine</button>
                    </div>
                </div>
                </div>
            </div>



         </div>)
     };
    render() { 
        return ( <div>
        
{/* <button className="btn btn-primary btn-sm" onClick={()=>this.props.saveDisease("Anup")}>Click</button> */}
<div className="row">
<div className="col-md-4"></div>
<div className="col-md-3 col-sm-3">
                <h3 className="text-dark">Search For Diseases</h3>
            </div>
</div>
    <div className="row form-group">
    <div className="col-md-4 col-sm-3"></div>
            
            <div className="col-md-3 col-sm-12">
								<select onChange={this.onChangeDisease} className="form-control" id="category"  data-toggle="tooltip" data-placement="top" title="Choose Category">
								<option>-- Choose Disease --</option>
                                {this.props.disease.response.map(function (item,index){
                   return (<option  key={index} value={item.disease}  >{item.disease}</option>)
                })}
                </select>
			</div>

    </div>
    <div className="row">
    <div className="col-md-1"></div>
    <div className="col-md-10 col-sm-11">
    {this.disaplyDiscription()}
    </div>
    
    </div>
<Footer/>
  
        </div> );
    }
}
 
const mapStateToProps=(state)=>{
    return {
        user:state.userReducer,
        disease:state.disease,
        hospital:state.hospital
    };
};

const mapDispatchToProps=(dispatch)=>{
    return {
      
        saveDisease:(skill)=>{
            console.log("SKILL is ",skill);
            dispatch({
                type:"SET_Response",
                payload:{
                    disease:"Fever",
                    drugsList:[{
                        drugName:"Drug 1",
                        ingredients:[
                            "abc",
                            "123"
                        ],
                    },
                    {
                        drugName:"Drug 2",
                        ingredients:[
                            "xyz",
                            "000"
                        ],
                    }

                ],
                    doAnddont:" Do and Dont will be displayd"
                        // name:"Fever",
                        // medicineName:["abc","xyz"], 
                        // description:"Fever is when a human's body temperature goes above the normal range of 36–37° Centigrade (98–100° Fahrenheit). It is a common medical sign",
                        // do:"Take Tablets",
                        // dont:"Dont take bath"
                }
            });
        },
        setName:(name)=>{
            console.log("Setting skills");
            dispatch({
                type:"SET_NAME",
                payload:name
            });
        },
        saveHospital:()=>{
            
            dispatch({
                type:"SAVE_HOSPITAL",
                payload:{
                    hospital:{
                        name:"APPOLO11111",
                        pinCode:"590102AAA",
                        contact:"0828823233",
                        mapURL:"",
                        site:""
            
                    }
                }
            })
        }
    };
};
export default connect(mapStateToProps,mapDispatchToProps) (SearchDiseases);
