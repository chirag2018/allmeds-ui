import React, { Component } from 'react';
import {Link} from "react-router";
import PatientDetail from "./user/patientDetails";

class Header extends Component {
    state = {  }
    render() { 
        return (
        <React.Fragment>
            <nav className="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
  <a className="navbar-brand" href="/userDetail">
  <img className="form-group" src="https://www.nalashaa.com/wp-content/uploads/2018/04/nes-logo.png" width="140px" height="40px"/> 
  </a>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
  
    <div className="navbar-nav">
      <Link className="nav-item nav-link active" to={"/validate"}>Validate <span className="sr-only">(current)</span></Link>
      <Link className="nav-item nav-link" to={"/userDetail"} >User Detail</Link>
      <Link  className="nav-item nav-link  mr-sm-2" to={"/search"} >Diseases</Link>
    </div>
  </div>
</nav>

        </React.Fragment>
            );
    }
}
 
export default Header;