import React, { Component } from 'react';
import Header from './header';

class Root extends Component {
    state = {  }
    render() { 
        return (
            <div>
                <div className="col-md-12">
                <Header/>        
                </div>

        <div className="row">
            <div className="col-md-12">
                {this.props.children}        
             </div>
        </div>
        
        </div>
         );
    }
}
export default Root;