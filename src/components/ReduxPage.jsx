import React, { Component } from 'react';
import {createStore, combineReducers,applyMiddleware} from  "redux";
import ReduxApp from "./ReduxApp";
import {Provider} from "react-redux";
import store from "../store";
import ReachDiseases from "./searchDiseases";

// const mathReducer=(state={
//     result:1,
//     lastValues:[]
// },action)=>{
//     switch(action.type){
//         case "ADD":
//             state={
//                 ...state,
//                 result:state.result+action.payload,
//                 lastValues:[...state.lastValues,action.payload]
//             };
//             break;
//     }
//     return state;
// };

// const skillReducer=(state={
//     skill:[]
// },action)=>{
// switch(action.type){
//     case "SAVE_SKILL":
//         state={
//             ...state,
//             skill:[...state.skill,action.payload]
//         };
//         break;
    
// }
// return state;
// };

// const MyLogger=(store)=>(next)=>(action)=>{
//     console.log(" Log Action : ",action);
//     next(action);
// }

// const store=createStore(combineReducers(
//     {skillReducer}),
//     {},
//     applyMiddleware(MyLogger)
//     );
    store.subscribe(()=>{
        console.log("State Update :",store.getState());
    });

    // store.dispatch({
    //     type:"SET_Response",
    //     payload:{
    //                  name:"Asthma",
    //                     medicineName:["123","789"], 
    //                     description:"Asthma is caused by inflammation of the small tubes, called bronchi, which carry air in and out of the lungs. If you have asthma, the bronchi will be inflamed and more sensitive than normal.",
    //                     do:"Take Asthma Tablets",
    //                     dont:"Dont take bath"
    //     }
    // })


        store.dispatch({
        type:"SET_Response",
        payload:{
                    disease:"Asthma",
                    drugsList:[{
                        drugName:"Drug 11",
                        ingredients:[
                            "sumo kold1",
                            "kil"
                        ],
                    },
                    {
                        drugName:"Drug 12",
                        ingredients:[
                            "ingree",
                            "sumo"
                        ],
                    }

                ],
                    doAnddont:" Do and Dont will be displayd"
                        // name:"Fever",
                        // medicineName:["abc","xyz"], 
                        // description:"Fever is when a human's body temperature goes above the normal range of 36–37° Centigrade (98–100° Fahrenheit). It is a common medical sign",
                        // do:"Take Tablets",
                        // dont:"Dont take bath"
                }
    })

    
    store.dispatch({
        type:"SET_Response",
        payload:{
                    disease:"Jaundice",
                    drugsList:[{
                        drugName:"Allen A18 Jaundice Drop",
                        ingredients:[
                            "Andrographis paniculata Q 1.00 ml",
                            "In Aqua Destillata"
                        ],
                    },
                    {
                        drugName:"Wheezal Sarsa Syrup",
                        ingredients:[
                            "none",
                            "none"
                        ],
                    }

                ],
                    doAnddont:"This medicine is used for Sluggish Liver & digestive functions and other related complaints."
                      
                }
    })

    store.dispatch({
        type:"SET_Response",
        payload:{
                    disease:"Heart Attack",
                    drugsList:[{
                        drugName:"Allen A18 Jaundice Drop",
                        ingredients:[
                            "Andrographis paniculata Q 1.00 ml",
                            "In Aqua Destillata"
                        ],
                    },
                    {
                        drugName:"Wheezal Sarsa Syrup",
                        ingredients:[
                            "none",
                            "none"
                        ],
                    }

                ],
                    doAnddont:"This medicine is used for Sluggish Liver & digestive functions and other related complaints."
                      
                }
    })


    // store.dispatch({
    //     type:"SET_Response",
    //     payload:{
                   
    //         "disease": null,
    //         "listOfPrescriptionDetails": [
    //             {
    //                 "medicineName": "Aspirin",
    //                 "compositionList": [
    //                     {
    //                         "id": 9,
    //                         "compositionName": "salicylic acid"
    //                     },
    //                     {
    //                         "id": 1,
    //                         "compositionName": "cytochrome P450"
    //                     },
    //                     {
    //                         "id": 8,
    //                         "compositionName": "antacids"
    //                     }
    //                 ],
    //                 "remarks": null
    //             },
    //             {
    //                 "medicineName": "Paracetamol",
    //                 "compositionList": [
    //                     {
    //                         "id": 4,
    //                         "compositionName": "magnesium stearate"
    //                     },
    //                     {
    //                         "id": 3,
    //                         "compositionName": "colloidal"
    //                     },
    //                     {
    //                         "id": 2,
    //                         "compositionName": "silica"
    //                     },
    //                     {
    //                         "id": 1,
    //                         "compositionName": "cytochrome P450"
    //                     }
    //                 ],
    //                 "remarks": null
    //             },
    //             {
    //                 "medicineName": "Crocin",
    //                 "compositionList": [
    //                     {
    //                         "id": 5,
    //                         "compositionName": "Maize Starch IP"
    //                     },
    //                     {
    //                         "id": 7,
    //                         "compositionName": "Potassium Sorbate BP/Ph Eur"
    //                     },
    //                     {
    //                         "id": 6,
    //                         "compositionName": "Stearic Acid IP"
    //                     }
    //                 ],
    //                 "remarks": null
    //             }
    //         ],
    //         "dosAndDonts": null

    //             }
    // })

    store.dispatch({
        type:"SAVE_HOSPITAL",
        payload:{
            hospital:{
                name:"APPOLO",
                pinCode:"590102",
                contact:"08023233",
                mapURL:"",
                site:""
    
            }
        }
    })
class ReduxPage extends Component {
    
    render() { 
        return (  <Provider store={store}>
            <ReachDiseases/>
          
        </Provider> );
    }
}
 
export default ReduxPage;