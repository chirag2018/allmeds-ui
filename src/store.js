import {createStore,combineReducers,applyMiddleware} from "redux";
import MyLogger from "./myLogger";
import userReducer from "./reducers/userReducer";
import disease from "./reducers/Diseases";
import hospital from "./reducers/hospitals";

const store=createStore(combineReducers(
    {userReducer,disease,hospital}),
    {},
    applyMiddleware(MyLogger)
    );

   
    export default store;