import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {render} from 'react-dom';
import {Router,Route,browserHistory} from "react-router";
import Root from "./components/root";
import PatientDetail from "./components/user/patientDetails";
import healthDeatil from "./components/user/healthDetail";
import stripValidate from "./components/user/validateSrtip";
import Login from "./components/user/login";
import SearchDiseases from "./components/searchDiseases";
import ReduxStore from "./components/ReduxPage";
import LoginHeader from "./components/logInHeader";
import userDetail from "./components/user/userDetail";

class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
         <Route path={"/"} component={Root}>
            <Route path={"healthDetail"} component={healthDeatil}/>
            <Route path={"validate"} component={stripValidate}/>
            <Route path={"search"} component={ReduxStore}/>
            <Route path={"userDetail"} component={userDetail}/>
          </Route>
          
          <Route path={"patientDetail"} component={PatientDetail}/>
              <Route path={"userLogin"} component={Login}/>
          <Route path={"/h"} component={LoginHeader}>
          </Route>

      </Router>
    );
  }
}

export default App;
